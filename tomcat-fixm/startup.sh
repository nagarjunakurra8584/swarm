#!/bin/bash -x

certs_dir=/home/dev/certs/
logs_dir=/home/dev/logs/

sudo cp -r ../certs/tomcat/ $certs_dir
sudo mkdir  $logs_dir

#############################   TOMCAT  ##########################################

cd $logs_dir 
sudo mkdir tomcat 
cd tomcat 
sudo  mkdir tomcat-fixm 
sudo chmod 777 tomcat-fixm 
sudo chown systemd-coredump tomcat-fixm
