#!/bin/bash

data_dir=/home/dev/data
certs_dir=/home/dev/certs
logs_dir=/home/dev/logs
configs_dir=/home/dev/configs
backup_dir=/home/dev/backup

sudo mkdir -p $certs_dir/openldap
sudo cp -r ../certs/openldap/* $certs_dir/openldap
sudo mkdir -p  $configs_dir
sudo mkdir -p  $logs_dir
sudo mkdir -p  $backup_dir
############################# OPENLDAP ###########################################
sudo mkdir -p $data_dir/openldap
cd $data_dir/openldap
sudo mkdir ldapA ldapB
cd $certs_dir/openldap
sudo chown systemd-coredump openldap_server.key
sudo chown systemd-coredump openldap_server.crt
sudo chown systemd-coredump rootCA.pem
sudo mkdir -p $configs_dir/openldap
cd $configs_dir/openldap
sudo mkdir ldapA ldapB
sudo mkdir -p $backup_dir/openldap
cd $backup_dir/openldap
sudo mkdir ldapA ldapB

########################## SECRETS  #################################################
printf Openldapadmin@123 | docker secret create ldap_admin_password -
printf config | docker secret create ldap_config_password -

