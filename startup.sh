#!/bin/bash -x

data_dir=/home/dev/data
certs_dir=/home/dev/certs
logs_dir=/home/dev/logs
configs_dir=/home/dev/configs

sudo cp -r ./data $data_dir
sudo cp -r ./certs $certs_dir
sudo mkdir  $logs_dir
sudo cp -r ./configs $configs_dir

################################  ETCD  ######################################

sudo mkdir $data_dir/etcd  && \
cd $data_dir/etcd  && \
sudo mkdir etcd-00 etcd-01 etcd-02 && \
sudo chmod -R 777 * && \
sudo chown -R systemd-coredump *

###############################  RABBITMQ #####################################

cd $data_dir/rabbitmq &&\
sudo chown -R systemd-coredump * &&\
cd $data_dir/rabbitmq/rabbitmq-node-1 &&\
sudo chmod 600 .erlang.cookie &&\
cd $data_dir/rabbitmq/rabbitmq-node-2 &&\
sudo chmod 600 .erlang.cookie &&\
cd $data_dir/rabbitmq/rabbitmq-node-3 &&\
sudo chmod 600 .erlang.cookie

cd $logs_dir &&\
sudo mkdir rabbitmq &&\
cd rabbitmq &&\
sudo mkdir rabbitmq-node-1 rabbitmq-node-2 rabbitmq-node-3 &&\
sudo chown  -R systemd-coredump *

############################## POSTGRESQL #####################################

sudo mkdir $data_dir/postgresql &&\
cd $data_dir/postgresql &&\
sudo mkdir patroni1 patroni2 patroni3 &&\
sudo chmod 777 patroni1 patroni2 patroni3 &&\
sudo chown  systemd-coredump patroni1 patroni2 patroni3

cd $certs_dir/postgresql/ &&\
sudo chown systemd-coredump:systemd-coredump rootCA.pem postgres_server.crt &&\
sudo chmod 600 postgres_server.key &&\
sudo chown 70:70 postgres_server.key

sudo mkdir $logs_dir/postgresql &&\
  cd  $logs_dir/postgresql &&\
sudo mkdir patroni1 patroni2 patroni3 &&\
sudo chmod -R 777 * &&\
sudo chown -R systemd-coredump *

#############################   TOMCAT  ##########################################

cd $logs_dir &&\
sudo mkdir tomcat &&\
  cd tomcat &&\
sudo  mkdir tomcat-iwxxm tomcat-fixm tomcat-dnotam security-gateway tomcat-swim-gateway &&\
sudo chmod 777 * &&\
sudo chown systemd-coredump *

############################# OPENLDAP ###########################################

sudo mkdir -p $data_dir/openldap
cd $data_dir/openldap
sudo mkdir ldapA ldapB
sudo mkdir -p $certs_dir/openldap
cd $certs_dir/openldap
sudo chown systemd-coredump openldap_server.key
sudo chown systemd-coredump openldap_server.crt
sudo chown systemd-coredump rootCA.pem
sudo mkdir -p $configs_dir/openldap
cd $configs_dir/openldap
sudo mkdir ldapA ldapB
sudo mkdir -p $backup_dir/openldap
cd $backup_dir/openldap
sudo mkdir ldapA ldapB

#############################  NGINX   ###########################################

cd $logs_dir &&\
sudo mkdir nginx-external-gateway nginx-internal-gateway &&\
sudo chmod 777 nginx-external-gateway nginx-internal-gateway &&\
sudo chown systemd-coredump nginx-external-gateway nginx-internal-gateway

########################### HAPROXY #############################################

sudo mkdir $logs_dir/haproxy

###########################  GRAFANA  #############################################

cd $data_dir/grafana &&\
sudo chmod 777 $data_dir/grafana

########################### PORTAINER ############################################

sudo mkdir $data_dir/portainer

########################## SECRETS  #################################################

printf administrator@123 | docker secret create rabbitmq_password -
printf admin | docker secret create rabbitmq_user -
printf Openldapadmin@123 | docker secret create ldap_admin_password -
printf config | docker secret create ldap_config_password -
printf admin | docker secret create grafana_user -
printf administrator@123 | docker secret create grafana_password -
printf postgres | docker secret create pg_exporter_user -
printf Postgresql@5435 | docker secret create pg_exporter_password -

################################### CONFIGS #########################################

cd $configs_dir/rabbitmq/
docker config create --template-driver golang rabbitmq.conf rabbitmq.conf

cd $configs_dir/nginx
docker config create --template-driver golang nginx-external.conf nginx-external.conf
docker config create --template-driver golang nginx-internal.conf nginx-internal.conf
docker config create --template-driver golang mime.types mime.types
docker config create --template-driver golang proxy.conf proxy.conf

cd $configs_dir/prometheus
docker config create --template-driver golang alertmanager.yml alertmanager.yml
docker config create --template-driver golang prometheus.yml prometheus.yml
docker config create --template-driver golang rules.yml rules.yml

cd $configs_dir/haproxy
docker config create --template-driver golang haproxy.cfg haproxy.cfg

cd $configs_dir/promtail
docker config create --template-driver golang config.yml config.yml

cd $configs_dir/loki
docker config create --template-driver golang local-config.yaml local-config.yaml

cd $configs_dir/postgresql
docker config create --template-driver golang patroni1.yml patroni1.yml
docker config create --template-driver golang patroni2.yml patroni2.yml
docker config create --template-driver golang patroni3.yml patroni3.yml

