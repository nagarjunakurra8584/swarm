#!/bin/bash -x

certs_dir=/home/dev/certs/
logs_dir=/home/dev/logs/

sudo cp -r ../certs/tomcat/ $certs_dir
sudo mkdir  $logs_dir

#############################   TOMCAT  ##########################################

cd $logs_dir 
sudo mkdir tomcat
  cd tomcat 
sudo  mkdir tomcat-swim-gateway 
sudo chmod 777 tomcat-swim-gateway
sudo chown systemd-coredump tomcat-swim-gateway
