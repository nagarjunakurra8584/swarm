#!/bin/bash
#pth=$(dirname $(pwd))
doc_ver=20.10.7
comp_ver=1.27.4
Docker(){
#x=$(dirname $(pwd))
sudo mkdir -p /home/$1/docker-rpm/docker-$3
sudo tar xvf $2/docker-$3.tar.gz -C /home/$1/docker-rpm/docker-$3 >/dev/null 2<&1
sudo rpm -ivh --nodeps --replacefiles --replacepkgs /home/$1/docker-rpm/docker-$3/*.rpm >/dev/null 2<&1
sudo sed -i '/ExecStart=/c\ExecStart=/usr/bin/dockerd -H unix:// --containerd=/run/containerd/containerd.sock' /usr/lib/systemd/system/docker.service
sudo systemctl daemon-reload
#sudo systemctl enable docker.service 2>dev/null
sudo systemctl start docker.socket
sudo systemctl start docker.service
sudo usermod -aG docker $USER
sts=`sudo systemctl status docker.service | grep -o failed`
if [ $sts=failed ];then
sudo systemctl restart docker.service
echo "docker is installed && `sudo docker --version`"
else
echo "docker is running"
echo "docker is installed && `sudo docker --version`"
fi
}
Compose(){
echo "Docker-compose installation started"
sudo mkdir -p /home/$1/docker-compose-files/docker-compose-$3/
sudo tar xvf $2/docker-compose-$3.tar.gz -C /home/$1/docker-compose-files/docker-compose-$3/
sudo cp /home/$1/docker-compose-files/docker-compose-$3/docker-compose  /usr/local/bin/
sudo chmod +x /usr/local/bin/docker-compose
echo " docker-compose is installed && `docker-compose --version` "
}
while IFS=',' read ip uname pth
 do
  src_host_ip=`hostname -I | awk '{ print $1 }'`
  if [ "$src_host_ip"  =  "$ip" ];then
     sudo docker --version | grep "Docker version" 2> /dev/null
     if [ $? -eq 0 ];then
      echo "docker is  already installed on $src_host_ip"
     else
      echo "docker installation started on: $ip"
      Docker $uname $pth $doc_ver
      sudo rm /usr/local/bin/docker-compose > /dev/null 2<&1
      Compose $uname $pth $comp_ver
      echo "Docker successfully installed on: $ip"
    fi
  else
#echo "sshpass package in src machine is need to install "
#ssh-keygen
#sshpass -p "$psw" ssh-copy-id "$uname"@"$ip"
 echo "docker installation started on: $ip"
ssh   "$uname"@"$ip" > /dev/null 2<&1 /bin/bash  <<EOF
which docker
if [ $? -eq 0 ];then
   sudo docker --version | grep "Docker version" 1> /dev/null
   if [ $? -eq 0 ];then
   echo "docker is already installed"
   else
   echo "docker installation started"
   $(typeset -f Docker)
   Docker $uname $pth $doc_ver
   fi
     ########### docker-compose ##########
     sudo docker-compose --version | grep "docker-compose version" 1>/dev/null
     if [ $? -eq 0 ];then
     echo "Docker-compose is already installed"
     else
     #echo "Docker-compose installation started"
     #sudo rm /usr/local/bin/docker-compose
      $(typeset -f Compose)
     Compose $uname $pth $comp_ver
     fi
else
echo "docker installation started on: $ip"
$(typeset -f Docker)
Docker $uname $pth $doc_ver
echo "docker-compose installation started on: $ip"
sudo rm /usr/local/bin/docker-compose >dev/null/ 2<&1
$(typeset -f Compose)
Compose $uname $pth $comp_ver
 echo "Docker successfully installed on: $ip"
fi
EOF
fi
done < $1
