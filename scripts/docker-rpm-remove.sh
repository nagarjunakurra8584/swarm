#!/bin/bash -x
Remove(){
echo "Started removing docker"
for pkg in $(rpm -qa | grep -E 'docker|container')
do
sudo rpm -e --nodeps $pkg
done
sudo rm -rf /var/lib/docker
sudo rm -rf /etc/docker
sudo rm -f /var/run/docker.sock
sudo rm -f /usr/lib/systemd/system/docker.service
sudo rm /usr/local/bin/docker-compose > /dev/null 2>&1
if [[ $(sudo which docker) && $(sudo docker --version) ]]; then
    echo "Docker is still available  "
  else
    echo "Docker uninstalled succesfully"
fi
}
while IFS=',' read ip user pth
do
src_host_ip=`hostname -I | awk '{ print $1}'`
if [ "$src_host_ip"  =  "$ip" ];then
Remove
else
ssh $user@$ip <<EOF
$(typeset -f Remove)
Remove
EOF
fi
done < $1
