#!/bin/bash
##### swarm init #####
mgr=$(grep -v '^#' $1 | head -n +1)
while IFS=',' read mip musr mpsw
do
MANAGER_IP=`hostname -I | awk '{ print $1 }'`
if [ "$MANAGER_IP"  =  "$mip" ];then
docker swarm init --advertise-addr $mip 1>/dev/null
else
ssh "$musr"@"$mip" /bin/bash <<EOF
docker swarm init --advertise-addr $mip
EOF
fi
done <<< $mgr
##### Adding nodes as manger in swarm #######
for i in $(grep -v '^#' $1 | sed -n '1d;p')
do
while IFS=',' read mip1 musr1 mpwd1
do
mgrip=$(awk -F ',' '{print $1}' <<<$mgr)
mgrusr=$(awk -F ',' '{print $2}' <<<$mgr)
locl_mchn_IP=`hostname -I | awk '{ print $1 }'`
if [ "$locl_mchn_IP"  =  "$mip1" ];then
#WorkerToken=$(ssh "$mgrusr"@"$mgrip" 'docker swarm join-token worker'| grep token | awk '{ print $5 }' )
ManagerToken=$(ssh "$mgrusr"@"$mgrip" 'docker swarm join-token manager' | grep token | awk '{ print $5 }')
docker swarm join --token ${ManagerToken} $mgrip:2377
else
mgrip=$(awk -F ',' '{print $1}' <<<$mgr)
ManagerToken=$(docker swarm join-token manager | grep token | awk '{ print $5 }')
#WorkerToken=$(docker swarm join-token worker| grep token | awk '{ print $5 }')
ssh "$musr1"@"$mip1" docker swarm join --token ${ManagerToken} ${mgrip}:2377
fi
done <<< $i
done

