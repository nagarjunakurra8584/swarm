#!/bin/bash -x

certs_dir=/home/dev/certs/
logs_dir=/home/dev/logs/
configs_dir=/home/dev/configs/

sudo cp -r ../certs/nginx/ $certs_dir
sudo mkdir  $logs_dir
sudo cp -r ../configs/nginx/ $configs_dir


#############################  NGINX   ###########################################

cd $logs_dir &&\
sudo mkdir nginx &&\
sudo chmod 777 nginx &&\
sudo chown systemd-coredump nginx

################################### CONFIGS #########################################


cd $configs_dir/nginx
docker config create --template-driver golang nginx.conf nginx.conf
docker config create --template-driver golang mime.types mime.types
docker config create --template-driver golang proxy.conf proxy.conf



