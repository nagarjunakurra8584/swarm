#!/bin/bash -x

certs_dir=/home/dev/certs
logs_dir=/home/dev/logs
configs_dir=/home/dev/configs
data_dir=/home/dev/data

sudo mkdir -p $logs_dir
sudo mkdir -p $data_dir
sudo mkdir -p $configs_dir
sudo mkdir -p $certs_dir
sudo cp -r ../certs/postgresql  $certs_dir
sudo cp -r ../configs/postgresql $configs_dir
sudo cp -r ../configs/haproxy $configs_dir

################################  ETCD  ######################################

sudo mkdir $data_dir/etcd  && \
cd $data_dir/etcd  && \
sudo mkdir etcd-00 etcd-01 etcd-02 && \
sudo chmod -R 777 * && \
sudo chown -R systemd-coredump *

############################## POSTGRESQL #####################################

sudo mkdir $data_dir/postgresql &&\
cd $data_dir/postgresql &&\
sudo mkdir patroni1 patroni2 patroni3 &&\
sudo chmod 777 patroni1 patroni2 patroni3 &&\
sudo chown  systemd-coredump patroni1 patroni2 patroni3

cd $certs_dir/postgresql/ &&\
sudo chown systemd-coredump:systemd-coredump rootCA.pem postgres_server.crt &&\
sudo chmod 600 postgres_server.key &&\
sudo chown 70:70 postgres_server.key

sudo mkdir $logs_dir/postgresql &&\
  cd  $logs_dir/postgresql &&\
sudo mkdir patroni1 patroni2 patroni3 &&\
sudo chmod -R 777 * &&\
sudo chown -R systemd-coredump *

################################### CONFIGS #########################################

cd $configs_dir/haproxy
docker config create --template-driver golang haproxy.cfg haproxy.cfg

cd $configs_dir/postgresql
docker config create --template-driver golang patroni1.yml patroni1.yml
docker config create --template-driver golang patroni2.yml patroni2.yml
docker config create --template-driver golang patroni3.yml patroni3.yml

