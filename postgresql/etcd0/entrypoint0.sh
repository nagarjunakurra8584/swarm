#!/bin/sh
/usr/local/bin/etcd \
--name=etcd-00 \
--data-dir=/data.etcd \
--advertise-client-urls=http://etcd-00:2379 \
--listen-client-urls=http://0.0.0.0:2379 \
--initial-advertise-peer-urls=http://etcd-00:2380 \
--listen-peer-urls=http://0.0.0.0:2380 \
--initial-cluster=etcd-00=http://etcd-00:2380,etcd-01=http://etcd-01:2380,etcd-02=http://etcd-02:2380 \
--initial-cluster-state=new \
--initial-cluster-token=etcd-cluster

