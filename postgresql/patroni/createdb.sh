#! /bin/sh
username=postgres
PGPASSWORD=Postgresql@5435 psql -h "$HOSTNAME" -U "$username" -p 5432 -c "CREATE DATABASE aixm";
PGPASSWORD=Postgresql@5435 psql -h "$HOSTNAME" -U "$username" -p 5432 -c "CREATE DATABASE fixm";
PGPASSWORD=Postgresql@5435 psql -h "$HOSTNAME" -U "$username" -p 5432 -c "CREATE DATABASE iwxxm";
PGPASSWORD=Postgresql@5435 psql -h "$HOSTNAME" -U "$username" -p 5432 -c "CREATE DATABASE security_gateway";
PGPASSWORD=Postgresql@5435 psql -h "$HOSTNAME" -U "$username" -p 5432 -c "CREATE DATABASE swim_gateway";
PGPASSWORD=Postgresql@5435 psql -h "$HOSTNAME" -U "$username" -p 5432 -c "CREATE DATABASE grafana";
PGPASSWORD=Postgresql@5435 psql -h "$HOSTNAME" -U "$username" -d aixm -p 5432 -c "CREATE EXTENSION postgis";
PGPASSWORD=Postgresql@5435 psql -h "$HOSTNAME" -U "$username" -d security_gateway -p 5432 -c "CREATE TABLE organization_metadata(org_metadata_id int primary key not null,org_type varchar(255))"
PGPASSWORD=Postgresql@5435 psql -h "$HOSTNAME" -U "$username" -d security_gateway -p 5432 -c "INSERT INTO organization_metadata values(1,'ANSP'),(2,'AU'),(3,'ASP'),(4,'ATSU'),(5,'OTHER')";
PGPASSWORD=Postgresql@5435 psql -h "$HOSTNAME" -U "$username" -d security_gateway -p 5432 -c "CREATE TABLE app_permission_metadata(app_permission_id bigint primary key not null ,app_type varchar(255),permission varchar(255))";
PGPASSWORD=Postgresql@5435 psql -h "$HOSTNAME" -U "$username" -d security_gateway -p 5432 -c "INSERT INTO app_permission_metadata values(1,'DNOTAM','CAN_QUERY_BASLINE'),(2,'DNOTAM','CAN_QUERY_EVENT'),(3,'DNOTAM','CAN_CREATE_EVENT'),(4,'DNOTAM','CAN_UPDATE_EVENT'),(5,'DNOTAM','CAN_QUERY_SEQUENCE'),(6,'DNOTAM','CAN_SUPERVISE'),(7,'DNOTAM','PERMISSION_ALL'),(8,'FFICE','CAN_FILE_FFICE_FLIGHT_PLAN'),(9,'FFICE','CAN_MODIFY_FFICE_FLIGHT_PLAN'),(10,'FFICE','CAN_ACCEPT_FIELD_FLIGHT_PLAN'),(11,'FFICE','CAN_NOTIFY_DELAY'),(12,'FFICE','CAN_NOTIFY_ALERT')"
PGPASSWORD=Postgresql@5435 psql -h "$HOSTNAME" -U "$username" -d security_gateway -p 5432 -c "INSERT INTO app_permission_metadata values(13,'FFICE','CAN_UPDATE_RCF'),(14,'FFICE','CAN_UPDATE_SPL'),(15,'FFICE','CAN_QUERY_FLIGHT_DATA'),(16,'FFICE','CAN_QUERY_BY_ANSP'),(17,'FFICE','PERMISSION_ALL'),(18,'DNOTAM','CAN_CREATE_BRIEFING')"

