#!/bin/bash -x

data_dir=/home/dev/data/
certs_dir=/home/dev/certs/
logs_dir=/home/dev/logs/
configs_dir=/home/dev/configs/

sudo mkdir $data_dir
sudo cp -r ../data/rabbitmq/ $data_dir
sudo mkdir $certs_dir
sudo cp -r ../certs/rabbitmq/ $certs_dir
sudo mkdir $logs_dir
sudo mkdir $configs_dir
sudo cp -r ../configs/rabbitmq/ $configs_dir

###############################  RABBITMQ #####################################

cd $data_dir/rabbitmq &&\
sudo chown -R systemd-coredump * &&\
cd $data_dir/rabbitmq/rabbitmq-node-1 &&\
sudo chmod 600 .erlang.cookie &&\
cd $data_dir/rabbitmq/rabbitmq-node-2 &&\
sudo chmod 600 .erlang.cookie &&\
cd $data_dir/rabbitmq/rabbitmq-node-3 &&\
sudo chmod 600 .erlang.cookie

cd $logs_dir &&\
sudo mkdir rabbitmq &&\
cd rabbitmq &&\
sudo mkdir rabbitmq-node-1 rabbitmq-node-2 rabbitmq-node-3 &&\
sudo chown  -R systemd-coredump *

########################## SECRETS  #################################################

printf administrator@123 | docker secret create rabbitmq_password -
printf admin | docker secret create rabbitmq_user -

################################### CONFIGS #########################################

cd $configs_dir/rabbitmq/
docker config create --template-driver golang rabbitmq.conf rabbitmq.conf
